package org.example.elu;

public class EluEconome extends Elu{

    EluEconome(String n, String p){
        super(n,p);
    }

    void depenseDotation(int montant){
        for(Personne p:this.assistants){
            if(montant>1480){
                p.addSous(1480);
                montant = montant - 1480;
            }
        }
    }
}
