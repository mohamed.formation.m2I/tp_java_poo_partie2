package org.example.elu;

public class EluEscroc extends Elu{
    int compteSuisse;
    EluEscroc(String n, String p){
        super(n,p);
        this.compteSuisse = 0;
    }
    void depenseDotation(int montant){
        for(Personne p:this.assistants){
            if(montant>1480){
                p.addSous(1480);
                montant = montant - 1480;
            }
        }
        if(montant>0) this.compteSuisse = this.compteSuisse + montant;
    }

}
