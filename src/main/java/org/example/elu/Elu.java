package org.example.elu;

import java.util.ArrayList;

public class Elu extends Personne{

    public ArrayList<Personne> assistants;
    Elu(String n, String p){
        super(n,p);
        this.assistants = new ArrayList<Personne>();
    }
    void embaucheAssistant(String n, String p){
        this.assistants.add(new Personne(n,p));
    }
    void licencieAssistant(Personne a){
        this.assistants.remove(a);
    }
    void depenseDotation(int montant) {
        for (Personne p : this.assistants) p.addSous(montant / this.assistants.size());


    }

}
