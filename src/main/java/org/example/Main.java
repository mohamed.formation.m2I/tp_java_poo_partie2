package org.example;

import org.example.recettes.Ingredient;
import org.example.recettes.Plat;

public class Main {

    public static void main(String[] args) {

        Plat p = new Plat("Choucroute");
        p.addIngredient(new Ingredient("choucroute","cuite",500,"g"));
        p.addIngredient(new Ingredient("lard","cuit_entier",150,"g"));
        p.addIngredient(new Ingredient("saucisse","cuite_entiere",2,
                "unite"));

    }

}