package org.example.onf;

import java.util.ArrayList;

public class Parcelle {
    private ArrayList<Arbre> arbres;
    private int surface;

    public Parcelle(int s) {
        this.arbres = new ArrayList<Arbre>();
        this.surface = s;
    }
    public void addArbre(Arbre a) {
        this.arbres.add(a);
    }
    public void setSurface(int s) {

        this.surface = s;
    }
    public int getNbArbres() {
        return this.arbres.size();
    }
}
