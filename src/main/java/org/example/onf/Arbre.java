package org.example.onf;

public class Arbre {

    private int hauteur, diametre;
    public Arbre(int h, int d, Parcelle p) {
        this.hauteur = h;
        this.diametre = d;
        p.addArbre(this);
    }
}
