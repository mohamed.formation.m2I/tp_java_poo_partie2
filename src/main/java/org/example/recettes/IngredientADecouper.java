package org.example.recettes;

public class IngredientADecouper extends Ingredient{

    IngredientADecouper(String n, String e, int q, String unite) {
        super(n, e, q, unite);
    }

    void decouper(){
        this.etat = "decoupe";
    }
}
