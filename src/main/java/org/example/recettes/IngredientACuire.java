package org.example.recettes;

public class IngredientACuire extends Ingredient{


    IngredientACuire(String n, String e, int q, String unite) {
        super(n, e, q, unite);
    }

    void cuire(){
        this.etat = "cuit";
    }


}
