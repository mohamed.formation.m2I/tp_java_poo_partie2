package org.example.recettes;

public class Ingredient {


    String nom_aliment, etat;
    int quantite;
    String unite;

    public Ingredient(String n, String e, int q, String unite) {
        this.nom_aliment = n;
        this.etat = e;
        this.quantite = q;
        this.unite = unite;
    }


    public boolean equals(Object o) {
        return (o instanceof Ingredient) &&
                this.nom_aliment.equals(((Ingredient) o).nom_aliment) &&
                this.etat.equals(((Ingredient) o).etat);
    }

}