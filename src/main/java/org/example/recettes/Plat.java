package org.example.recettes;

import java.util.ArrayList;

public class Plat {


    public String nom;
    public ArrayList<Ingredient> ingredients;

    public Plat(String n) {
        this.nom = n;
        this.ingredients = new ArrayList<Ingredient>();
    }

    public String getNom() {
        return this.nom;
    }

    public ArrayList<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void addIngredient(Ingredient i) {
        this.ingredients.add(i);
    }


    public boolean equals(Object o) {
        if (o instanceof Plat) {
            for (Ingredient i : this.ingredients) {
                if (!((Plat) o).ingredients.contains(i)) return false;
            }
            return this.ingredients.size() ==
                    ((Plat) o).ingredients.size();
        } else return false;
    }

}
